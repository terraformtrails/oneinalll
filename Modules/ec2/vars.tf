variable "ami" {
  description = "amiID for my Singapore Instances"
}

variable "instance_type" {
  description = "instance type for my Singapore Instances"
}

variable "subnetID" {
  description = "Subnet for my Singapore Instances"
}

variable "ec2_count" {
  description = "Tell how many EC2 instance to be created"
}

variable "Instance_name" {
  description = "Name for my EC2 instance to be created"
}

