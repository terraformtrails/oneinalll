variable "region" {
  default = "ap-southeast-1"
}

variable "cidr_block" {
  description = "cidr_block for SingaporeVPC"
}

variable "instance_tenancy" {
  default = "dedicated"
  description = "instance_tenancy for SingaporeVPC"
}

variable "cidr_block_subnet" {
  description = "cidr_block_subnet for SingaporeSubnet"
}

variable "vpc_id" {
  description = "vpc id for my SingaporeVPC"
}

variable "VPC_name" {
  description = "VPC name for my SingaporeVPC"
}